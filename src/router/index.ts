import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    redirect: '/inicio',
  },
  {
    path: '/inicio',
    name: 'init',
    component: () => import('../views/Init.vue'),
  },
  {
    path: '/cancion',
    name: 'song',
    component: () => import('../views/Song.vue'),
  },
  {
    path: '/personaje',
    name: 'personage',
    component: () => import('../views/Personage.vue'),
  },
  {
    path: '/links',
    name: 'links',
    component: () => import('../views/Links.vue'),
  },
  {
    path: '/proyecto',
    name: 'project',
    component: () => import('../views/Project.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
